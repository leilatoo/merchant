# worldpay offer service

### Introduction

This is a RESTful Dropwizard web application written in Java 8 using Maven for build and dependency management. The application allows the user to create, retrieve and delete simple 'Offers' via the application API. 

### How to test the application

To run tests run the following command:

 ```mvn clean test```

### How to run the application

To run and manually test the application run the following commands:

* To package the application run the following from the root directory:

     ```mvn package```
     
* To run the server run the following from the root directory:

     ``` java -jar target/market-1.0-SNAPSHOT.jar server merchant.yml```
     
You can either curl input or use an application like Postman to test the end points. 
