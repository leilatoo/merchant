package com.worldpay.offer;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.util.Optional;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("offer")
public class OfferResource {

    private final OfferDao offerDao;

    public OfferResource(OfferDao offerDao) {
        this.offerDao = offerDao;
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response insert(Offer offer, @Context UriInfo uriInfo) {
        if (offer == null) {
            return Response.status(422).build();
        }

        int id = offerDao.insert(offer);
        return Response.created(uriInfo.getAbsolutePathBuilder().path("{id}").build(id)).build();
    }

    @GET
    @Path("{id}")
    @Produces(APPLICATION_JSON)
    public Response get(@PathParam("id") int id) {
        Optional<Offer> offer = offerDao.findById(id);
        if (!offer.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(offer.get()).build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") int id) {
        Optional<Offer> offer = offerDao.findById(id);
        if (!offer.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        offerDao.delete(offer.get());
        return Response.noContent().build();
    }

}
