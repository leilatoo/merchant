package com.worldpay.offer;


import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class OfferDao {

    private int id = 1;
    private Calendar calendar;
    private Map<Integer, Offer> offers = new HashMap<>();

    public OfferDao(Calendar calendar) {
        this.calendar = calendar;
    }

    public Optional<Offer> findById(int id) {
        return (offerExists(id) && offerIsValid(id))
                ? Optional.of(offers.get(id))
                : Optional.empty();
    }

    public int insert(Offer offer) {
        Offer withId = new Offer(getNextId(), offer.getGoods(), offer.getDescription(), offer.getPrice(), offer.getExpires());
        offers.put(id, withId);
        id++;
        return withId.getId();
    }

    public void delete(Offer offer) {
        offers.remove(offer.getId());
    }

    private int getNextId() {
        return id;
    }

    private boolean offerIsValid(int id) {
        return offers.get(id).getExpires().isAfter(today());
    }

    private boolean offerExists(int id) {
        return offers.containsKey(id);
    }

    private LocalDate today() {
        return calendar.today();
    }

}
