package com.worldpay.offer;

import java.time.LocalDate;

public class Calendar {

    public LocalDate today() {
        return LocalDate.now();
    }

}
