package com.worldpay.offer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class Offer implements Serializable {

    private int id;
    private String goods;
    private String description;
    private int price;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate expires;

    private Offer() {
        // for deserialization
    }

    public Offer(String goods, String description, int price, LocalDate expires) {
        this.goods = goods;
        this.description = description;
        this.price = price;
        this.expires = expires;
    }

    public Offer(int id, String goods, String description, int price, LocalDate expires) {
        this.id = id;
        this.goods = goods;
        this.description = description;
        this.price = price;
        this.expires = expires;
    }

    public int getId() {
        return id;
    }

    public String getGoods() {
        return goods;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public LocalDate getExpires() {
        return expires;
    }
}
