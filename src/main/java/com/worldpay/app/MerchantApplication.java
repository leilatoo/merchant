package com.worldpay.app;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.worldpay.offer.Calendar;
import com.worldpay.offer.OfferDao;
import com.worldpay.offer.OfferResource;

public class MerchantApplication extends Application<MerchantApplicationConfiguration> {

    public static void main(String[] args) throws Exception {
        new MerchantApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<MerchantApplicationConfiguration> bootstrap) { }

    @Override
    public void run(MerchantApplicationConfiguration merchantApplicationConfiguration, Environment environment) {

        final Calendar calendar = new Calendar();
        final OfferDao offerDao = new OfferDao(calendar);

        final OfferResource offerResource = new OfferResource(offerDao);
        environment.jersey().register(offerResource);
    }

}
