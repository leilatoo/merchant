package com.worldpay.offer;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;

import java.time.LocalDate;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class OfferTest {

    private static LocalDate DATE = LocalDate.of(2017, 10, 31);
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private final Offer offer = new Offer(1, "kitten", "super cute", 10, DATE);


    @Test
    public void serializesToJSON() throws Exception {

        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/offer.json"), Offer.class));

        assertThat(MAPPER.writeValueAsString(offer), is(expected));
    }

    @Test
    public void serializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/offer.json"), Offer.class), is(offer));
    }

}