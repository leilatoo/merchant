package com.worldpay.offer;

import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OfferResourceTest {

    private OfferDao offerDao = mock(OfferDao.class);
    private OfferResource offerResource = new OfferResource(offerDao);

    @Rule
    public final ResourceTestRule resource = ResourceTestRule.builder()
            .addResource(offerResource)
            .build();

    private static LocalDate TOMORROW = LocalDate.now().plusDays(1);

    final Offer offer = new Offer( "kitten", "super cute", 10, TOMORROW);
    final Offer offerWithId = new Offer( 1, "puppy", "adorable", 8, TOMORROW);

    @Test
    public void insert_shouldReturn201AndOfferId() {
        when(offerDao.insert(offer)).thenReturn(42);

        Response insertResponse = resource.client().target("/offer")
                .request()
                .post(Entity.json(offer));

        assertThat(insertResponse.getStatus(), is(201));
        assertThat(insertResponse.getHeaderString("Location"), is("http://localhost:9998/offer/42"));

        verify(offerDao, times(1)).insert(offer);
    }

    @Test
    public void insert_shouldReturn422IfOfferIsNull() {
        Response response = resource.client().target("/offer")
                .request()
                .post(Entity.json(null));

        assertThat(response.getStatus(), is(422));
        verifyZeroInteractions(offerDao);
    }

    @Test
    public void get_shouldReturnOfferIfExists() {
        when(offerDao.findById(1)).thenReturn(Optional.of(offer));

        assertThat(resource.target("/offer/1").request().get(Offer.class), is(offer));

        verify(offerDao, times(1)).findById(1);
    }

    @Test
    public void get_shouldReturn404IfOfferDoesNotExist() {
        when(offerDao.findById(42)).thenReturn(Optional.empty());

        Response response = resource.client().target("/offer/42")
                .request()
                .get();

        assertThat(response.getStatus(), is(404));
        verify(offerDao, times(1)).findById(42);
    }


    @Test
    public void delete_shouldReturn204AndDeleteExistingOffer() {
        when(offerDao.findById(1)).thenReturn(Optional.of(offerWithId));

        Response response = resource.client().target("/offer/1")
                .request()
                .delete();

        assertThat(response.getStatus(), is(204));
        verify(offerDao, times(1)).findById(1);
        verify(offerDao, times(1)).delete(offerWithId);
    }

    @Test
    public void delete_shouldReturn404IfOfferDoesNotExist() {
        when(offerDao.findById(42)).thenReturn(Optional.empty());

        Response response = resource.client().target("offer/42")
                .request()
                .delete();

        assertThat(response.getStatus(), is(404));
        verifyNoMoreInteractions(offerDao);
    }

}