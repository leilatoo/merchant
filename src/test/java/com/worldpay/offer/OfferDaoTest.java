package com.worldpay.offer;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class OfferDaoTest {

    private static LocalDate TODAY = LocalDate.now();
    private static LocalDate TOMORROW = TODAY.plusDays(1);
    private static LocalDate YESTERDAY = TODAY.minusDays(1);

    private final Offer newOffer = new Offer( "puppy", "adorable", 8, TOMORROW);
    private final Offer expiredOffer = new Offer( "kitten", "super cute", 10, YESTERDAY);

    @Mock
    Calendar calendar;

    OfferDao offerDao;

    @Before
    public void initialise() {
        offerDao = new OfferDao(calendar);
    }

    @Test
    public void insert_shouldCreateAndStoreAnOffer() {
        given(calendar.today()).willReturn(TODAY);

        int id = offerDao.insert(newOffer);

        Offer offerWithId = new Offer(id, newOffer.getGoods(), newOffer.getDescription(), newOffer.getPrice(), newOffer.getExpires());

        assertThat(offerDao.findById(1).get(), is(offerWithId));
    }

    @Test
    public void findById_shouldReturnOnlyValidOffers() {
        given(calendar.today()).willReturn(TODAY);

        int id = offerDao.insert(expiredOffer);

        assertThat(offerDao.findById(id), is(Optional.empty()));
    }

    @Test
    public void delete_shouldDeleteOffer() {
        given(calendar.today()).willReturn(TODAY);

        int id = offerDao.insert(newOffer);
        Offer offerWithId = new Offer(id, newOffer.getGoods(), newOffer.getDescription(), newOffer.getPrice(), newOffer.getExpires());
        offerDao.delete(offerWithId);

        assertThat(offerDao.findById(id), is(Optional.empty()));
    }

}