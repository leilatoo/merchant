package com.worldpay.offer;

import com.worldpay.app.MerchantApplication;
import com.worldpay.app.MerchantApplicationConfiguration;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.time.LocalDate;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class OfferAcceptanceTest {

    @ClassRule
    public static final DropwizardAppRule<MerchantApplicationConfiguration> RULE = new DropwizardAppRule<>(
            MerchantApplication.class, "merchant.yml"
    );

    private Client client = new JerseyClientBuilder().register(RULE.getEnvironment()).build();

    private static LocalDate TOMORROW = LocalDate.now().plusDays(1);
    private static LocalDate YESTERDAY = LocalDate.now().minusDays(1);
    private final Offer newOffer = new Offer( "puppy", "adorable", 8, TOMORROW);
    private final Offer expiredOffer = new Offer( "kitten", "super cute", 10, YESTERDAY);


    @Test
    public void insert_ShouldReturn201AndOfferId() throws JsonProcessingException {

        Response postResponse = client.target(String.format("http://localhost:%d/offer", RULE.getLocalPort()))
                .request()
                .post(Entity.json(newOffer));

        assertThat(postResponse.getStatus(), is(201));

        int id = Integer.valueOf(getIdFromLocation(postResponse));

        Response getResponse = client.target(String.format("http://localhost:%d/offer/%d", RULE.getLocalPort(), id))
                .request()
                .get();

        assertThat(postResponse.getStatus(), is(201));
        assertThat(getResponse.readEntity(Offer.class),
                is(new Offer(id, newOffer.getGoods(), newOffer.getDescription(), newOffer.getPrice(), newOffer.getExpires())));
    }


    @Test
    public void get_ShouldReturn404IfOfferDoesNotExist() {

        Response response = client.target(String.format("http://localhost:%d/offer/42", RULE.getLocalPort()))
                .request()
                .get();

        assertThat(response.getStatus(), is(404));
    }

    @Test
    public void get_ShouldReturn404IfOfferExistsButHasExpired() {

        Response postResponse = client.target(String.format("http://localhost:%d/offer", RULE.getLocalPort()))
                .request()
                .post(Entity.json(expiredOffer));

        int id = Integer.valueOf(getIdFromLocation(postResponse));

        Response getResponse = client.target(String.format("http://localhost:%d/offer/%d", RULE.getLocalPort(), id))
                .request()
                .get();

        assertThat(getResponse.getStatus(), is(404));
    }


    @Test
    public void delete_ShouldReturn204AndDeleteOfferIfExists() {

        Response postResponse = client.target(String.format("http://localhost:%d/offer", RULE.getLocalPort()))
                .request()
                .post(Entity.json(newOffer));

        int id = Integer.valueOf(getIdFromLocation(postResponse));

        Response deleteResponse = client.target(String.format("http://localhost:%d/offer/%d", RULE.getLocalPort(), id))
                .request()
                .delete();

        assertThat(deleteResponse.getStatus(), is(204));
    }


    private static String getIdFromLocation(Response response) {
        String location = getLocationFromHeader(response);
        return location.substring(location.lastIndexOf("/") + 1, location.length());
    }

    private static String getLocationFromHeader(Response response) {
        return response.getHeaderString("Location");
    }


}
